const BTN = document.querySelector(".OFForON");
let IMG = document.querySelector(".bulb");
let state = true;
function onBulb() {
    IMG.src = "images/pic_bulbon.gif";
    // console.log("on");
}
function offBulb() {
    IMG.src = "images/pic_bulboff.gif";
    // console.log("off");
}

function offORon() {
    if (state) {
        onBulb()
        console.log("off");
        state = false;
    }
    else if (!state) {
        offBulb()
        console.log("on");
        state = true;
    }
}

BTN.addEventListener("click", offORon);
